{% extends 'cours.twig.html' %}

{% block cours %}
<div class="cours">
    <h2> Le modèle de VON NEUMANN </h2>
    <div class="alert alert-secondary">
        <p class="mb-0">
            Le contenu de cette page est largement extrait de la newsletter interstice à l'adresse
            <a href="https://interstices.info/le-modele-darchitecture-de-von-neumann/">
                https://interstices.info/le-modele-darchitecture-de-von-neumann/
            </a> que vous pouvez consulter si vous souhaitez aller plus loin. C'est le travail de
            Sacha Krakowiak,
            Professeur émérite d'informatique à l'Université Joseph Fourier.
        </p>
    </div>
    <h3> Naissance du modèle </h3>
    <p>
        En 1944, John von Neumann est introduit dans le projet ENIAC par Herman Goldstine, qui assurait la liaison
        scientifique du projet avec le département de la Défense. Von Neumann était un esprit universel, dont les
        contributions allaient des mathématiques à la logique, la physique et l’économie. Il avait rencontré Turing et
        connaissait ses travaux. Il participait alors au projet Manhattan, et l’Histoire dit que Goldstine lui parla du
        projet ENIAC lors d’une rencontre fortuite sur un quai de gare. Quoi qu’il en soit, von Neumann accepta un poste
        de consultant dans ce projet et prit une part active aux travaux menés par Eckert et Mauchly sur la conception
        de l’EDVAC (Electronic Discrete Variable Automatic Computer), le successeur de l’ENIAC. En juin 1945, une
        première version, incomplète, d’un rapport sur la conception de l’EDVAC fut diffusée par Goldstine, sous la
        signature du seul von Neumann qui l’avait rédigé comme document de travail. Eckert et Mauchly en furent, à juste
        titre, profondément choqués ; par ailleurs, ils entrèrent en conflit avec l’Université de Pennsylvanie pour des
        questions de brevet et ces deux circonstances provoquèrent leur départ du projet en mars 1946 pour fonder leur
        entreprise, l’Eckert-Mauchly Computer Corporation. Von Neumann lui-même quitta le projet à la même époque pour
        Princeton, où il travailla avec Julian Bigelow sur le calculateur de l’IAS (Institut d’Études Avancées)
    </p>
    <h3>Une architecture novatrice</h3>
    <p>
        Le First Draft of a Report on EDVAC est un document de cent-une pages, qui décrit d’une part un schéma
        d’architecture de calculateur, organisé en trois éléments (unité arithmétique, unité de commande et mémoire
        contenant programme et données), d’autre part des principes de réalisation pour ces éléments, notamment les
        opérations arithmétiques. Si ce dernier aspect dépend partiellement de la technologie connue à l’époque, et a
        donc nécessairement vieilli, le modèle d’architecture, qui marque une transition profonde avec les pratiques
        antérieures, reste d’une étonnante actualité. Ce modèle, auquel reste attaché le nom de von Neumann, est
        représenté par le schéma ci-dessous.
    </p>
    <div class="d-flex">
        <figure class="figure mx-auto">
            <img class="figure-img img-fluid img-rounded img-thumbnail"
                src="ressources/architecture/assets/img/modele_originel_neumann.gif">
            <figcaption class="figure-caption text-center"> Le modèle originel de von Neumann pour l’architecture des
                ordinateurs. </figcaption>
        </figure>
    </div>

    <p>
        La première innovation est la séparation nette entre l’unité de commande, qui organise le flot de séquencement
        des instructions, et l’unité arithmétique, chargée de l’exécution proprement dite de ces instructions. La
        seconde innovation, la plus fondamentale, est l’idée du programme enregistré : les instructions, au lieu d’être
        codées sur un support externe (ruban, cartes, tableau de connexions), sont enregistrées dans la mémoire selon un
        codage conventionnel. Un compteur ordinal contient l’adresse de l’instruction en cours d’exécution ; il est
        automatiquement incrémenté après exécution de l’instruction, et explicitement modifié par les instructions de
        branchement.
    </p>
    <p>
        Un emplacement de mémoire peut contenir indifféremment des instructions et des données, et une conséquence
        majeure (dont toute la portée n’avait probablement pas été perçue à l’époque) est qu’un programme peut être
        traité comme une donnée par d’autres programmes. Cette idée, présente en germe dans la machine de Turing,
        trouvait ici sa concrétisation.
    </p>
    <h3> Qu'en est il aujourd'hui ? </h3>
    <p>
        Plus de soixante ans après son invention, le modèle d’architecture de von Neumann régit toujours l’architecture
        des ordinateurs. Par rapport au schéma initial, on peut noter deux évolutions.
    </p>
    <div class="d-flex">
        <figure class="figure mx-auto">
            <img class="figure-img img-fluid img-rounded img-thumbnail"
                src="ressources/architecture/assets/img/modele_actuel_neumann.gif">
            <figcaption class="figure-caption text-center"> Le modèle de von Neumann, aujourd'hui. </figcaption>
        </figure>
    </div>
    <ul>
        <li>
            Les entrées-sorties, initialement commandées par l’unité centrale, sont depuis le début des années 1960 sous
            le contrôle de processeurs autonomes (canaux d’entrée-sortie et mécanismes assimilés). Associée à la
            multiprogrammation (partage de la mémoire entre plusieurs programmes), cette organisation a notamment permis
            le développement des systèmes en temps partagé.
        </li>
        <li>
            Les ordinateurs comportent maintenant des processeurs multiples, qu’il s’agisse d’unités séparées ou de «
            cœurs » multiples à l’intérieur d’une même puce. Cette organisation permet d’atteindre une puissance globale
            de calcul élevée sans augmenter la vitesse des processeurs individuels, limitée par les capacités
            d’évacuation de la chaleur dans des circuits de plus en plus denses.
        </li>
    </ul>
    <p>
        Ces deux évolutions ont pour conséquence de mettre la mémoire, plutôt que l’unité centrale, au centre de
        l’ordinateur, et d’augmenter le degré de parallélisme dans le traitement et la circulation de l’information.
        Mais elles ne remettent pas en cause les principes de base que sont la séparation entre traitement et commande
        et la notion de programme enregistré.
    </p>
    <p>
        L’accès des processeurs à la mémoire se fait à travers un bus (non représenté sur la figure), voie d’échange
        assurant un transfert rapide de l’information. Mais au cours du temps, et pour des raisons technologiques, le
        débit du bus a crû moins vite que le débit d’accès à la mémoire et surtout que la vitesse des processeurs. D’où
        un phénomène d’attente — le « goulot de von Neumann » — qui réduit les performances. Des palliatifs sont l’usage
        généralisé de caches à plusieurs niveaux (mémoire d’accès rapide, voisine du processeur, et retenant les données
        courantes), et le développement de machines à mémoire distribuée mais se posent alors des problèmes de cohérence
        pour les données en copies multiples.
    </p>
</div>
{% endblock %}