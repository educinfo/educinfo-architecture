<?php

register_activity(
    'architecture',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1hardos',
        'type' => 'url',
        'titre' => "Architecture des ordinateurs",
        'auteur' => "Laurent COOPER",
        'URL' => 'index.php?activite=architecture&page=introduction',
        'commentaire' => "La fabrication des ordinateurs est une prouesse technique. Elle repose aussi sur des concepts théoriques passionnants.",
        'directory' => 'architecture',
        'icon' => 'fas fa-cogs',
        'prerequis' => NULL
    )
);
