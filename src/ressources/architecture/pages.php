<?php
/*****************
1ère NSI
Architecture des ordinateurs
******************/
global $pages;

// Bases du javascript
$menu_activite = array(
    "titre" => "Architecture",
    "contenu" => [
        'introduction',
        'modele_von_neumann',
        'portes_logiques'
    ]
);

// pages des bases du javascript
$architecture_pages = array(
    'introduction' => array(
        "template" => 'architecture/introduction.twig.html',
        "menu" => 'architecture',
        'page_precedente' => NULL,
        'page_suivante' => 'modele_von_neumann',
        'titre' => 'Introduction',
        "css" => 'ressources/architecture/assets/css/architecture.css'
    ),
    'modele_von_neumann' => array(
        "template" => 'architecture/Modele_Von_Neumann.twig.html',
        "menu" => 'architecture',
        'page_precedente' => 'introduction',
        'page_suivante' => 'portes_logiques',
        'titre' => 'Le modèle de VON NEUMANN',
        "css" => 'ressources/architecture/assets/css/architecture.css'
    ),
    'portes_logiques' => array(
        "template" => 'architecture/portes_logiques.twig.html',
        "menu" => 'architecture',
        'page_precedente' => 'modele_von_neumann',
        'page_suivante' => NULL,
        'titre' => 'Transistors et portes logiques',
        "css" => 'ressources/architecture/assets/css/architecture.css'
    ),
);

$pages = array_merge($pages, $architecture_pages);
