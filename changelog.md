# Changelog
*educinfo-architecture*

Package pour l'apprentissage des notions d'architecture des ordinateurs (1ère et terminale).

Tous les changements notables seront documentés dans ce fichier

Le format s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnement sémantique](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Ajout
- ce changelog
- structure des répertoires
- page.php avec le sommaire
- ajout des pages sur l'historique et le modèle de Von NEUMANN
- squelette de page sur les portes logiques
- différentes images